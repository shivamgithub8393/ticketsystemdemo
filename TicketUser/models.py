from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class Organiztion_Category(models.Model):
    Organiztion_Category_Title = models.CharField(max_length=500)
    Organiztion_Category_Choices = (
        ('Active', 'ACTIVE'), ('Inactive', 'INACTIVE'))
    Organiztion_Category_Status = models.CharField(
        max_length=20, choices=Organiztion_Category_Choices)
    Organiztion_Category_Created_Date = models.DateField(
        auto_now_add=True, blank=True, null=True)
    Organiztion_Category_Last_Updated_Date = models.DateField(
        auto_now=True, blank=True, null=True)

    class Meta:
        verbose_name_plural = "List of All Organiztion_Category"

    def __str__(self):
        return self.Organiztion_Category_Title
    
    
class Related_Area(models.Model):
    Related_Area_Title = models.CharField(max_length=50)
    Related_Area_Choices = (('Active', 'ACTIVE'), ('Inactive', 'INACTIVE'))
    Related_Area_Status = models.CharField(
        max_length=20, choices=Related_Area_Choices)
    Related_Area_Created_Date = models.DateField(auto_now_add=True)
    Related_Area_Last_Updated_Date = models.DateField(auto_now_add=True)
   
    class Meta:
        verbose_name_plural = "Related Area"

    def __str__(self):
        return self.Related_Area_Title
    
class Technology_Stack(models.Model):
    Technology_Stack_Name = models.CharField(max_length=500)
    Technology_Stack_Choices = (('Active', 'ACTIVE'), ('Inactive', 'INACTIVE'))
    Technology_Stack_Status = models.CharField(
        max_length=20, choices=Technology_Stack_Choices)
    Technology_Stack_Creation_Date = models.DateField(auto_now_add=True, blank=True, null=True)
    Technology_Stack_Last_Updated_Date = models.DateField(auto_now_add=True)
   
    class Meta:
        verbose_name_plural = "List of Technology Stack"

    def __str__(self):
        return self.Technology_Stack_Name


class TicketStatus(models.Model):
    status = models.CharField(max_length=50)

    class Meta:
        verbose_name = "TicketStatus"
        verbose_name_plural = "TicketStatus"

    def __str__(self):
        return self.status


class UserRoleMapping(models.Model):
    user_id = models.ForeignKey(User, verbose_name="USer", on_delete=models.CASCADE)
    role_name = models.CharField(choices=(('admin', 'Admin'),('agent', 'Agent'),('user', 'User')), max_length=50)
    
    class Meta:
        verbose_name = "UserRoleMapping"
        verbose_name_plural = "UserRoleMapping"

    def __str__(self):
        return self.role_name
    

class Agent(models.Model):
    user_id = models.ForeignKey(User, verbose_name="User ID", on_delete=models.CASCADE)
    agent_name = models.CharField(max_length=50)
    agent_related_area = models.ForeignKey(Related_Area, on_delete=models.CASCADE)
    agent_technical_stack = models.ManyToManyField(Technology_Stack)
    agent_creation_date = models.DateField(auto_now_add=True)
    agent_contact_number = models.CharField(max_length=50)
    agent_status = models.CharField(choices=(('active', 'ACTIVE'), ('inactive', 'INACTIVE')), max_length=50)

    class Meta:
        verbose_name = "Agent"
        verbose_name_plural = "Agent"

    def __str__(self):
        return self.agent_name
    
    
class Ticket(models.Model):
    ticket_reference_number = models.CharField(max_length=100, blank=True, null=True)
    ticket_title = models.CharField(max_length=50, blank=True, null=True)
    ticket_desc = models.TextField(max_length=50, blank=True, null=True)
    ticket_user_uploaded_document = models.FileField(upload_to="TicketUser/ticket_user_documents", blank=True, null=True)
    organization_name = models.CharField(max_length=50, blank=True, null=True)
    organization_website_url = models.CharField(max_length=50, blank=True, null=True)
    organization_category = models.ForeignKey(Organiztion_Category, on_delete=models.CASCADE)
    ticket_related_area = models.ForeignKey(Related_Area, on_delete=models.CASCADE)
    ticket_technology_stack = models.ManyToManyField(Technology_Stack)
    ticket_raised_by = models.ForeignKey(User, verbose_name="User", on_delete=models.CASCADE)
    ticket_status = models.ForeignKey(TicketStatus, verbose_name="Ticket Status", on_delete=models.CASCADE)
    technical_person_name = models.CharField(max_length=50, blank=True, null=True)
    technical_person_email = models.EmailField(max_length=254, null=True, blank=True)
    technical_person_contact_number = models.CharField(max_length=50, blank=True, null=True)
    ticket_created_date = models.DateField(auto_now_add=True, blank=True, null=True)
    ticket_user_last_updated_date = models.DateField(auto_now=True, blank=True, null=True)
    
    class Meta:
        verbose_name = "Ticket"
        verbose_name_plural = "Ticket"

    def __str__(self):
        return self.ticket_title + ", ID: " + str(self.id)
      
class ClosedTicket(models.Model):
    ticket_id = models.ForeignKey(Ticket, verbose_name="Ticket ID", on_delete=models.CASCADE)
    agent_closing_remark = models.TextField(max_length=50, blank=True, null=True)
    admin_closing_remark = models.TextField(max_length=50, blank=True, null=True)
    system_closing_remark = models.TextField(max_length=50, blank=True, null=True)
    closing_date = models.DateField(auto_now_add=True, blank=True, null=True)
    
    class Meta:
        verbose_name = "ClosedTicket"
        verbose_name_plural = "ClosedTicket"

    def __str__(self):
        return str(self.ticket_id)


class EscalatedTicket(models.Model):
    ticket_id = models.ForeignKey(Ticket, verbose_name="Ticket ID", on_delete=models.CASCADE)
    agent_id = models.ForeignKey(Agent, verbose_name="Agent email", on_delete=models.CASCADE)
    agent_to_admin_remark = models.TextField(max_length=50, blank=True, null=True)
    escalation_date = models.DateField(auto_now_add=True, blank=True, null=True)
    
    class Meta:
        verbose_name = "EscalatedTicket"
        verbose_name_plural = "EscalatedTicket"

    def __str__(self):
        return str(self.ticket_id)
    
      
class AdminAction(models.Model):
    ticket_id = models.ForeignKey(Ticket, verbose_name="tikcet ID", on_delete=models.CASCADE)
    agent_id = models.ForeignKey(Agent, verbose_name="Agent email", on_delete=models.CASCADE)
    # ticket_status_to_agent = models.ForeignKey(TicketStatus, verbose_name="Ticket Status", on_delete=models.CASCADE)
    admin_to_agent_remark = models.TextField(max_length=254, null=True, blank=True)
    agent_ticket_assigned_date =  models.DateField(auto_now_add=True, blank=True, null=True)
    # agent_tkt_update_date =  models.DateField(auto_now=True , blank=True, null=True)

    class Meta:
        verbose_name = "AdminAction"
        verbose_name_plural = "AdminAction"

    def __str__(self):
        return self.ticket_id.ticket_reference_number
    

class AgentAction(models.Model):
    ticket_id = models.ForeignKey(Ticket, verbose_name="tikcet ID", on_delete=models.CASCADE)
    agent_id = models.ForeignKey(Agent, verbose_name="Agent email", on_delete=models.CASCADE)
    agent_action = models.ForeignKey(TicketStatus, verbose_name="Ticket Status", on_delete=models.CASCADE)
    agent_ticket_action_date =  models.DateField(auto_now_add=True, blank=True, null=True)
    
    class Meta:
        verbose_name = "AgentAction"
        verbose_name_plural = "AgentAction"

    def __str__(self):
        return str(self.id)
    
    
class AgentResponse(models.Model):
    agent_action_id = models.ForeignKey(AgentAction, verbose_name="Agent Action ID", on_delete=models.CASCADE)
    agent_to_admin_remark = models.TextField(max_length=254, null=True, blank=True)
    agent_to_user_remark = models.TextField(max_length=254, null=True, blank=True)
    agent_ticket_upload_document = models.FileField(upload_to="TicketUser/ticket_user_documents", blank=True, null=True)
    

    class Meta:
        verbose_name = "AgentResponse"
        verbose_name_plural = "AgentResponse"

    def __str__(self):
        return str(self.agent_action_id)


class Reopen(models.Model):
    ticket_id = models.ForeignKey(Ticket, verbose_name="tikcet ID", on_delete=models.CASCADE)
    user_ticket_reopen_data_description = models.TextField(max_length=254, null=True, blank=True)
    user_ticket_reopen_docs = models.FileField(upload_to="TicketUser/ticket_user_reopen_documents", blank=True, null=True)
    user_ticket_reopen_date = models.DateField(auto_now_add=True, blank=True, null=True)
    user_ticket_action_on_agent_response = models.ForeignKey(TicketStatus, verbose_name="Ticket Status", on_delete=models.CASCADE)
    agent_action_id = models.ForeignKey(AgentAction, verbose_name="Agent Action ID", on_delete=models.CASCADE)
    
    class Meta:
        verbose_name = "Reopen"
        verbose_name_plural = "Reopen"

    def __str__(self):
        return self.user_ticket_reopen_data_description