from .models import *
from django import forms
from django.contrib.auth.models import User

class LoginForm(forms.Form):
    username = forms.EmailField(max_length=60, required=True, widget=forms.EmailInput(
        attrs={'class': 'form-control', 'id': 'inputEmail', 'placeholder': 'Enter Email', 'autocomplete': 'off'}))

    password = forms.CharField(required=True, strip=False, widget=forms.PasswordInput(
        attrs={'class': 'form-control', 'id': 'inputPassword', 'placeholder': 'Enter Password', 'name': 'password'}))


# Ticket details
class Ticket_Form(forms.ModelForm):
    class Meta:
        model = Ticket
        fields = [
          'ticket_title','ticket_desc','ticket_user_uploaded_document','organization_name',
          'organization_website_url','organization_category','ticket_related_area',
          'ticket_technology_stack','technical_person_name','technical_person_email',
          'technical_person_contact_number'
          ]
        
        
# admin Action details
class AdminActionForm(forms.Form):
    agent_id = forms.ModelChoiceField(queryset=Agent.objects.all())
    admin_to_agent_remark = forms.CharField(required=False)
        
        
# Agent Action details
class AgentActionForm(forms.Form):
    agent_to_admin_remark = forms.CharField(widget=forms.Textarea, required=False)
    agent_to_user_remark = forms.CharField(widget=forms.Textarea, required=False)
    agent_ticket_upload_document = forms.FileField(required=False)
    agent_action = forms.ModelChoiceField(queryset=TicketStatus.objects.all())
        
        
# reopen details
class ReopnForm(forms.Form):
    user_ticket_reopen_data_description = forms.CharField(widget=forms.Textarea)
    user_ticket_reopen_docs = forms.FileField(required=False)

        