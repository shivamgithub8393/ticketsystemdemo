from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(UserRoleMapping)
admin.site.register(Agent)
admin.site.register(TicketStatus)
admin.site.register(Ticket)
admin.site.register(ClosedTicket)
admin.site.register(AdminAction)
admin.site.register(AgentAction)
admin.site.register(AgentResponse)
admin.site.register(Reopen)

admin.site.register(Organiztion_Category)
admin.site.register(Technology_Stack)
admin.site.register(Related_Area)