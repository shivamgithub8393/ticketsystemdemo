import random
import string


def generate_TKT_ID():
  randomLetter = "".join(random.choices(string.ascii_uppercase, k=3))
  random_number = random.randint(100000000,999999999)

  TKT_ID = "TKT"+randomLetter+str(random_number)
  return TKT_ID