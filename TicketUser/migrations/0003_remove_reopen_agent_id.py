# Generated by Django 4.2.2 on 2023-06-15 09:12

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('TicketUser', '0002_rename_ticket_status_to_user_ticket_ticket_status'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='reopen',
            name='agent_id',
        ),
    ]
