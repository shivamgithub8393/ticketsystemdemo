from django.shortcuts import render, redirect
from .forms import *
from django.contrib.auth import authenticate, login, logout
from .models import *
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .utils import *

# Create your views here.
def home(request):
  return render(request, 'TicketUser/home.html')


@login_required()
def user_dashboard(request):
  data = Ticket.objects.filter(ticket_raised_by=request.user)
  return render(request, 'TicketUser/user_dashboard.html', {'data': data})


@login_required()
def agent_dashboard(request):
  data = AdminAction.objects.filter(agent_id__user_id=request.user)
  # ticket_data = []
  # for tickets in data:
  #   Ticket.objects.get(tickets.ticket_id)
    
  return render(request, 'TicketUser/agent_dashboard.html', {'data': data})


@login_required()
def admin_dashboard(request):
  data = Ticket.objects.all()
  return render(request, 'TicketUser/admin_dashboard.html', {'data': data})


def user_login(request):
  print("in login view")

  if request.method == 'POST':
    form = LoginForm(request.POST)
    username = request.POST.get('username')
    password = request.POST.get('password')
    user = authenticate(username=username, password=password)
    
    print("User ", user)
    if user:
      user_role = UserRoleMapping.objects.get(user_id=user)
      print("role ", user_role.role_name)
      
      login(request, user)
      
      if user_role.role_name == 'agent':
        return redirect('TicketUser:agent_dashboard')
      elif user_role.role_name == 'user':
        return redirect('TicketUser:user_dashboard')
      else:
        return redirect('TicketUser:admin_dashboard')
    else:
      print("no user found")
      messages.error(request, "Incorrect login credentials", extra_tags="danger")
  else:
    form = LoginForm()
  return render(request, 'TicketUser/user_login.html', {'form':form})


@login_required()
def user_logout(request):
  logout(request)
  
  return redirect("TicketUser:user_login")


@login_required()
def create_ticket(request):
  
  if request.method == 'POST':
    form = Ticket_Form(request.POST, request.FILES)
    if form.is_valid():
    
      temp_obj = form.save(commit=False)
      # update 
      # ticket_reference_number
      # ticket_raised_by
      # ticket_status
      # generate ticket ID
      
      TKT_ID = generate_TKT_ID()
      while Ticket.objects.filter(ticket_reference_number=TKT_ID).exists():
          print("exists and ticket ", TKT_ID)
          TKT_ID = generate_TKT_ID()
      print("Generated Ticket number ", TKT_ID)
      
      ticket_status_pending_obj = TicketStatus.objects.get(
            status='open')
      
      temp_obj.ticket_reference_number = TKT_ID
      temp_obj.ticket_raised_by = request.user
      temp_obj.ticket_status = ticket_status_pending_obj
      
      temp_obj.save()
      form.save_m2m()
      messages.success(request, "Ticket Added Successfully", extra_tags='success')
    else:
      messages.success(request, "Invalid Data", extra_tags='danger')
  
  else:
    form = Ticket_Form()

  
  return render(request, 'TicketUser/create_ticket.html', {'form':form})


@login_required()
def ticket_list(request):

  
  return render(request, 'TicketUser/ticket_list.html')


@login_required()
def admin_action(request, id):
  ticket_obj = Ticket.objects.get(id=id)
  if request.method == 'POST':
    form = AdminActionForm(request.POST, request.FILES)
    
    if form.is_valid():
      ticket_data = Ticket.objects.get(id=id)
      
      if ticket_data.ticket_status.status == 'open' or ticket_data.ticket_status.status == 'escalated':
        agent_id = request.POST.get('agent_id')
        admin_to_agent_remark = request.POST.get('admin_to_agent_remark')
        
        admin_action = AdminAction.objects.create(ticket_id=ticket_obj, agent_id=Agent.objects.get(id=agent_id), admin_to_agent_remark=admin_to_agent_remark)
        admin_action.save()
        
        ticket_status_pending_obj = TicketStatus.objects.get(status='assigned')
        ticket_data.ticket_status = ticket_status_pending_obj
        ticket_data.save()
        messages.success(request, "Admin assigned ticket to agent", extra_tags='success')
      else:
        messages.success(request, "Already assigned", extra_tags='danger')
    else:
      messages.success(request, "Invalid Data", extra_tags='danger')
  else:
    agent_list = Agent.objects.all()
    form = AdminActionForm(initial={'agent_id': agent_list})
  
  return render(request, 'TicketUser/admin_action.html', {'form': form, 'ticket_obj': ticket_obj})


# No USE
@login_required()
def agent_action(request, id):
  ticket_details = Ticket.objects.get(id=id)
  user = request.user
  agent_details = Agent.objects.get(user_id=user)
  
  if request.method == 'POST':
    form = AgentActionForm(request.POST, request.FILES)
    
    if form.is_valid():
      form.save()
      
      status_selected = request.POST.get('agent_action')
      print("action ", status_selected)
      
      # update ticket status for user
      ticket_data = Ticket.objects.get(id=id)
      ticket_status_pending_obj = TicketStatus.objects.get(id=status_selected)
      ticket_data.ticket_status = ticket_status_pending_obj
      ticket_data.save()
      
      # update ticket status for agent
      admin_action = AdminAction.objects.get(ticket_id=id, agent_id=agent_details)
      ticket_status_pending_obj = TicketStatus.objects.get(id=status_selected)
      admin_action.ticket_status_to_agent = ticket_status_pending_obj
      admin_action.save()
      
      messages.success(request, "Agent response given", extra_tags='success')
    else:
      messages.success(request, "Invalid Data", extra_tags='danger')
    
    
  else:
    form = AgentActionForm(initial={'ticket_id':ticket_details, 'agent_id':agent_details})
  
  return render(request, 'TicketUser/agent_action.html', {'form': form, 'ticket_details': ticket_details})


@login_required()
def reopen(request, id):
  ticket_details = Ticket.objects.get(id=id)
  
  if ticket_details.ticket_status.status == 'resolved' or ticket_details.ticket_status.status == 'insufficient data':
  
    if request.method == 'POST':
      form = ReopnForm(request.POST,  request.FILES)
      
      if form.is_valid():
        user_ticket_reopen_data_description = request.POST.get('user_ticket_reopen_data_description')
        user_ticket_reopen_docs = request.POST.get('user_ticket_reopen_docs')
        
        if ticket_details.ticket_status.status == 'insufficient data':
          new_status = 'data provided'
        else:
          new_status = 'reopen'
          
        reopen_obj = Reopen.objects.create(
          ticket_id=ticket_details,
          user_ticket_reopen_data_description=user_ticket_reopen_data_description,
          user_ticket_reopen_docs=user_ticket_reopen_docs,
          user_ticket_action_on_agent_response=TicketStatus.objects.get(status=new_status),
          agent_action_id=AgentAction.objects.filter(ticket_id=ticket_details).last()
        )
        reopen_obj.save()
        
        # update  ticket status for user
        ticket_data = Ticket.objects.get(id=id)
        ticket_status_pending_obj = TicketStatus.objects.get(status=new_status)
        ticket_data.ticket_status = ticket_status_pending_obj
        ticket_data.save()
        
        messages.success(request, "Reopen data submitted", extra_tags='success')
        return redirect('TicketUser:user_view_ticket', id=id)
      else:
        messages.success(request, "Invalid Data", extra_tags='danger')
    else:
      form = ReopnForm()
  else:
    messages.success(request, "Ticket can not be reopen", extra_tags='danger')
    return redirect('TicketUser:user_view_ticket', id=ticket_details.id)
    

  
  return render(request, 'TicketUser/reopen.html', {'form': form, 'ticket_details': ticket_details})


@login_required()
def admin_view_ticket(request, id):
  
  ticket_details = Ticket.objects.get(id=id)
  print(ticket_details)
  
  # agent = Agent.objects.get(user_id=request.user)
  admin_action = AdminAction.objects.filter(ticket_id=id).last()
  agent_resp = AgentAction.objects.filter(ticket_id=id)
  
  
  all_data = []
  for resp in agent_resp:
    if resp.agent_action.status == 'attending':
      all_data.append({"agent_resp": resp})
    else:
      agent_response = AgentResponse.objects.get(agent_action_id=resp.id)
      user_resp = Reopen.objects.filter(agent_action_id=resp.id)
      if user_resp:
        print("user resp found")
        all_data.append({"agent_action": resp, "agent_resp": agent_response, 'user_resp': user_resp[0]})
      else:
        print('no user resp[]')
        all_data.append({"agent_action": resp, "agent_resp": agent_response,})
  
  # all_data = []
  # for resp in agent_resp:
  #   user_resp = Reopen.objects.filter(agent_action_id=resp.id)
  #   if user_resp:
  #     all_data.append({"agent_resp": resp, 'user_resp': user_resp[0]})
  #   else:
  #     all_data.append({"agent_resp": resp})
  
  print(all_data)
    
  
  return render(request, 'TicketUser/admin_view_ticket.html', {"ticket_data": ticket_details, 'data': all_data, 'admin_action': admin_action})


@login_required()
def agent_view_ticket(request, id):

      
  if request.method == 'POST':
    form = AgentActionForm(request.POST, request.FILES)
    
    if form.is_valid():
      agent_to_admin_remark = request.POST.get('agent_to_admin_remark')
      agent_to_user_remark = request.POST.get('agent_to_user_remark')
      agent_ticket_upload_document = request.POST.get('agent_ticket_upload_document')
      agent_action_id = request.POST.get('agent_action')
      
      print("action ", agent_action_id)
      
      ticket_details = Ticket.objects.get(id=id)
      agent_details = Agent.objects.get(user_id=request.user)
      
      if ticket_details.ticket_status.status != 'attending':
        messages.error(request, "Please attend ticket first", extra_tags='danger')
        return redirect('TicketUser:agent_view_ticket', id=ticket_details.id)
      else:
        # add entry for agent reply
        agent_action = AgentAction.objects.create(ticket_id=ticket_details , agent_id=agent_details , agent_action=TicketStatus.objects.get(id=agent_action_id))
        agent_action.save()
        agent_resp = AgentResponse.objects.create(agent_action_id=agent_action, agent_to_admin_remark=agent_to_admin_remark , agent_to_user_remark=agent_to_user_remark , agent_ticket_upload_document=agent_ticket_upload_document)
        agent_resp.save()
        
        # update ticket status for user
        ticket_data = Ticket.objects.get(id=id)
        ticket_status_pending_obj = TicketStatus.objects.get(id=agent_action_id)
        ticket_data.ticket_status = ticket_status_pending_obj
        ticket_data.save()
        
        messages.success(request, "Agent response given", extra_tags='success')
        return redirect('TicketUser:agent_view_ticket', id=ticket_details.id)
    else:
      messages.error(request, "Invalid Data", extra_tags='danger')
  else:
    form = AgentActionForm()
  
  # update ticket status after change
  ticket_details = Ticket.objects.get(id=id)
  agent_resp = AgentAction.objects.filter(ticket_id=id)
  admin_action = AdminAction.objects.filter(ticket_id=id).last()
  # all_data = []
  # for resp in agent_resp:
  #   agent_response = AgentResponse.objects.get(agent_action_id=resp.id)
  #   user_resp = Reopen.objects.filter(agent_action_id=resp.id)
  #   if user_resp:
  #     all_data.append({"agent_action": resp, "agent_resp": agent_response, 'user_resp': user_resp[0]})
  #   else:
  #     all_data.append({"agent_action": resp, "agent_resp": agent_response})
  all_data = []
  for resp in agent_resp:
    if resp.agent_action.status == 'attending':
      all_data.append({"agent_resp": resp})
    else:
      agent_response = AgentResponse.objects.get(agent_action_id=resp.id)
      user_resp = Reopen.objects.filter(agent_action_id=resp.id)
      if user_resp:
        print("user resp found")
        all_data.append({"agent_action": resp, "agent_resp": agent_response, 'user_resp': user_resp[0]})
      else:
        print('no user resp[]')
        all_data.append({"agent_action": resp, "agent_resp": agent_response,})
  
  return render(request, 'TicketUser/agent_view_ticket.html', {"ticket_data": ticket_details, 'data': all_data, 'admin_action': admin_action, 'form': form})


@login_required()
def user_view_ticket(request, id):
  
  ticket_details = Ticket.objects.get(id=id)
  print(ticket_details)
  
  agent_resp = AgentAction.objects.filter(ticket_id=id)

  all_data = []
  for resp in agent_resp:
    if resp.agent_action.status == 'attending':
      all_data.append({"agent_resp": resp})
    else:
      agent_response = AgentResponse.objects.get(agent_action_id=resp.id)
      user_resp = Reopen.objects.filter(agent_action_id=resp.id)
      if user_resp:
        print("user resp found")
        all_data.append({"agent_action": resp, "agent_resp": agent_response, 'user_resp': user_resp[0]})
      else:
        print('no user resp[]')
        all_data.append({"agent_action": resp, "agent_resp": agent_response,})
  
  print(all_data)
    
  
  return render(request, 'TicketUser/user_view_ticket.html', {"ticket_data": ticket_details, 'data': all_data})


@login_required()
def close_ticket(request, id):
  ticket_status_pending_obj = TicketStatus.objects.get(status='closed')
  ticket_data = Ticket.objects.get(id=id)
  
  if request.method == 'POST':
    agent_closing_remark = request.POST.get('agent_closing_remark')
    admin_closing_remark = request.POST.get('admin_closing_remark')
    system_closing_remark = request.POST.get('system_closing_remark')
    
    closed_ticket_obj = ClosedTicket.objects.create(ticket_id=ticket_data, agent_closing_remark=agent_closing_remark, admin_closing_remark=admin_closing_remark, system_closing_remark=system_closing_remark)
    closed_ticket_obj.save()
  else:
    closed_ticket_obj = ClosedTicket.objects.create(ticket_id=ticket_data)
    closed_ticket_obj.save()
    
  # update ticket status for user
  ticket_data.ticket_status = ticket_status_pending_obj
  ticket_data.save()
  
  messages.success(request, "Ticket closed", extra_tags='success')
  return redirect('TicketUser:user_view_ticket', id=id)


@login_required()
def attended_view(request, id):
  tkt_obj = Ticket.objects.get(id=id)
  agent_obj = Agent.objects.get(user_id=request.user)
  ticket_status_attending = TicketStatus.objects.get(status='attending')
  
  agent_action_instance = AgentAction.objects.create(ticket_id=tkt_obj, agent_id=agent_obj, agent_action=ticket_status_attending)
  agent_action_instance.save()

  # update ticket status for user
  ticket_data = Ticket.objects.get(id=id)
  ticket_data.ticket_status = ticket_status_attending
  ticket_data.save()
  
  # # update ticket status for agent
  # agent = Agent.objects.get(user_id=request.user)
  # admin_action = AdminAction.objects.get(ticket_id=id, agent_id=agent)
  # admin_action.ticket_status_to_agent = ticket_status_attending
  # admin_action.save()
  
  messages.success(request, "Ticket attended", extra_tags='success')
  return redirect('TicketUser:agent_view_ticket', id=id)