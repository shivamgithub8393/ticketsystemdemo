from django.apps import AppConfig


class TicketuserConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'TicketUser'
