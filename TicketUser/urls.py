from django.urls import path
from .views import *

app_name = 'TicketUser'

urlpatterns = [
    path('', user_login, name='home'),

    path('login/', user_login, name='user_login'),
    path('user_logout/', user_logout, name='user_logout'),
    path('user_dashboard/', user_dashboard, name='user_dashboard'),
    path('agent_dashboard/', agent_dashboard, name='agent_dashboard'),
    path('admin_dashboard/', admin_dashboard, name='admin_dashboard'),
    path('create_ticket/', create_ticket, name='create_ticket'),
    path('ticket_list/', ticket_list, name='ticket_list'),
    path('admin_action/<id>', admin_action, name='admin_action'),
    path('agent_action/<id>', agent_action, name='agent_action'),
    path('reopen/<id>', reopen, name='reopen'),
    path('user_view_ticket/<id>', user_view_ticket, name='user_view_ticket'),
    path('agent_view_ticket/<id>', agent_view_ticket, name='agent_view_ticket'),
    path('admin_view_ticket/<id>', admin_view_ticket, name='admin_view_ticket'),
    path('close_ticket/<id>', close_ticket, name='close_ticket'),
    path('attended_view/<id>', attended_view, name='attended_view'),
]